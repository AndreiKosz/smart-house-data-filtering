
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Admin on 20.05.2017.
 */
public class GetStream {

    private String path;
    private List<String> logs;
    public GetStream(String path){
        this.path=path;
    }


    public List<MonitoredData> stream(){

        List<Date>startTime=new ArrayList<>();
        List<Date>endTime=new ArrayList<>();
        List<String>acitivities=new ArrayList<>();
        List<MonitoredData> monitoredData=new ArrayList<>();
        DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);


        try {
                    logs = Files.lines(Paths.get(path))
                    .map(line -> line.split("\t")).flatMap(Arrays::stream)
                    .map(String::valueOf)
                    .collect(Collectors.toList());

            for(int i=0;i<logs.size();i+=5) {
                Date startDate=format.parse(logs.get(i));
                Date endDate=format.parse(logs.get(i+2));
                startTime.add(startDate);
                endTime.add(endDate);
                acitivities.add(logs.get(i + 4));
            }

            for(int i=0;i<acitivities.size();i++){
                MonitoredData monitoredData1=new MonitoredData(startTime.get(i),endTime.get(i),acitivities.get(i));
                monitoredData.add(monitoredData1);

            }


        }
        catch(IOException|ParseException e){
            e.printStackTrace();
        }

        return monitoredData;
    }


}
