import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Admin on 20.05.2017.
 */
public class MonitoredData {

    private Date startDate;
    private Date endDate;
    private String activityLabel;
    private List<MonitoredData> monitoredData = new ArrayList<>();

    public MonitoredData(Date startDate, Date endDate, String activityLabel) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.activityLabel = activityLabel;

    }

    public MonitoredData(){

    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Date getStartDate() {
        return startDate;
    }



    public long countDays() {
        List<MonitoredData>monitoredDatas;
        GetStream getStream = new GetStream("E:/Faculta/ActivityLog/src/Activity.txt");
        monitoredDatas=getStream.stream();
        long sum=monitoredDatas.stream().filter(d->d.getStartDate().getDay()!=d.getEndDate().getDay()).count()+1;


        return sum;
    }

    public void distincActions(){
        List<MonitoredData>monitoredDatas;
        GetStream getStream=new GetStream("E:/Faculta/ActivityLog/src/Activity.txt");
        monitoredDatas=getStream.stream();
        Map<String,Long> counts=monitoredDatas.stream()
                .collect(Collectors.groupingBy(d->d.getActivityLabel(),Collectors.counting()));

        try{
            PrintWriter writer = new PrintWriter("Numbers.txt", "UTF-8");
            writer.println(counts);
            writer.close();
        } catch (FileNotFoundException|UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public int getDay(){
        Calendar cal=Calendar.getInstance();
        cal.setTime(this.getStartDate());
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    public void distinctActionDaily(){

        List<MonitoredData>monitoredDatas1;
        GetStream getStream=new GetStream("E:/Faculta/ActivityLog/src/Activity.txt");
        monitoredDatas1=getStream.stream();

        Map<Integer,Map<String,Long>> counts=monitoredDatas1.stream()
                .collect(Collectors.groupingBy(d->d.getDay(),Collectors.groupingBy(d->d.getActivityLabel(),Collectors.counting())));

        try{
            PrintWriter writer = new PrintWriter("Day.txt", "UTF-8");
            writer.println(counts);
            writer.close();
        } catch (FileNotFoundException|UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public void longActions(){
        List<MonitoredData>monitoredDatas1;
        GetStream getStream=new GetStream("E:/Faculta/ActivityLog/src/Activity.txt");
        monitoredDatas1=getStream.stream();

        Map<String,Long> actions=monitoredDatas1.stream()
                .collect(Collectors.groupingBy(d->d.getActivityLabel(),Collectors.summingLong(d->getDateDiff(d.getStartDate(),d.getEndDate(),TimeUnit.MINUTES))));

        Map<String,Long>actions2=actions.entrySet().stream()
                .filter(d->d.getValue()>600)
                .collect(Collectors.toMap(d->d.getKey(),d->d.getValue()/60));

        try{
            PrintWriter writer = new PrintWriter("MoreThan10H.txt", "UTF-8");
            writer.println(actions2);
            writer.close();
        } catch (FileNotFoundException|UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void shortActions(){

        List<MonitoredData>monitoredDatas1;
        GetStream getStream=new GetStream("E:/Faculta/ActivityLog/src/Activity.txt");
        monitoredDatas1=getStream.stream();
        Map<String,Long>total;
        total=monitoredDatas1.stream()
                .collect(Collectors.groupingBy(d->d.getActivityLabel(),Collectors.counting()));

       Map<String,Long>total2=monitoredDatas1.stream()
               .collect(Collectors.groupingBy(d->d.getActivityLabel(),Collectors.summingLong(d->getDateDiff(d.getStartDate(),d.getEndDate(),TimeUnit.MINUTES))));

       Map<String,Long>total3=total2.entrySet().stream()
               .filter(d->d.getValue()<=300)
               .collect(Collectors.groupingBy(d->d.getKey(),Collectors.counting()));

        try{
            PrintWriter writer = new PrintWriter("Short.txt", "UTF-8");
            writer.println(total3);
            writer.close();
        } catch (FileNotFoundException|UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
